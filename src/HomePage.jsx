import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import {Page, Toolbar} from 'react-onsenui';
import { Header, ProfileDropdown, CountrySelectionList, CountrySelectionListItem,
         Button, Grid } from 'metro-ui-components';

export default class HomePage extends React.Component {
  constructor() {
    super();
    this.state = {orders: [], listing: false};
  }

  componentDidMount(){
  }



  listOrders(e){
    e.preventDefault();
    console.log(this);
    axios
      .get('http://api-k8s-001-test9-mcc-gb-lon1.metroscales.io:30084/orders')
      .then(res => {
        let ordersArray = res.data.map((order) => {
          return (
            <li key={order.orderNumber}>{order.orderNumber}</li>
          )
        })
        this.setState({ orders: ordersArray });
        console.log(this.state.orders)

      })
      .catch(err => console.log('err', err))
  }

  render() {
    var self = this
    return (
      <Page>
        <Header>
          <ProfileDropdown options={[
            {name: "name1",  label: "Label 1"},
            {name: "name2",  label: "Label 2"},
            {name: "name3",  label: "Label 3"}]} />
        </Header>

        <Button onClick={this.listOrders.bind(this)}>List Orders</Button>

        <div>
          {this.state.orders.length? (
            <ul>
               {this.state.orders}
            </ul>
            ):(<div>
            <p>No Orders Found</p>
            </div>)
          }
        </div>
      </Page>
    );
  }
}
